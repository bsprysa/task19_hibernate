package com.sprysa;

import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "medicine", schema = "storedpr_db", catalog = "")
public class MedicineEntity {

  private int id;
  private String name;
  private String ministryCode;
  private Boolean recipe;
  private Boolean narcotic;
  private Boolean psychotropic;

  @Id
  @Column(name = "id", nullable = false)
  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  @Basic
  @Column(name = "name", nullable = false, length = 30)
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Basic
  @Column(name = "ministry_code", nullable = true, length = 10)
  public String getMinistryCode() {
    return ministryCode;
  }

  public void setMinistryCode(String ministryCode) {
    this.ministryCode = ministryCode;
  }

  @Basic
  @Column(name = "recipe", nullable = true)
  public Boolean getRecipe() {
    return recipe;
  }

  public void setRecipe(Boolean recipe) {
    this.recipe = recipe;
  }

  @Basic
  @Column(name = "narcotic", nullable = true)
  public Boolean getNarcotic() {
    return narcotic;
  }

  public void setNarcotic(Boolean narcotic) {
    this.narcotic = narcotic;
  }

  @Basic
  @Column(name = "psychotropic", nullable = true)
  public Boolean getPsychotropic() {
    return psychotropic;
  }

  public void setPsychotropic(Boolean psychotropic) {
    this.psychotropic = psychotropic;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    MedicineEntity that = (MedicineEntity) o;
    return id == that.id &&
        Objects.equals(name, that.name) &&
        Objects.equals(ministryCode, that.ministryCode) &&
        Objects.equals(recipe, that.recipe) &&
        Objects.equals(narcotic, that.narcotic) &&
        Objects.equals(psychotropic, that.psychotropic);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, ministryCode, recipe, narcotic, psychotropic);
  }
}
