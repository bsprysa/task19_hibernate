package com.sprysa;

import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "pharmacy_medicine", schema = "storedpr_db", catalog = "")
@IdClass(PharmacyMedicineEntityPK.class)
public class PharmacyMedicineEntity {

  private int pharmacyId;
  private int medicineId;

  @Id
  @Column(name = "pharmacy_id", nullable = false)
  public int getPharmacyId() {
    return pharmacyId;
  }

  public void setPharmacyId(int pharmacyId) {
    this.pharmacyId = pharmacyId;
  }

  @Id
  @Column(name = "medicine_id", nullable = false)
  public int getMedicineId() {
    return medicineId;
  }

  public void setMedicineId(int medicineId) {
    this.medicineId = medicineId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PharmacyMedicineEntity that = (PharmacyMedicineEntity) o;
    return pharmacyId == that.pharmacyId &&
        medicineId == that.medicineId;
  }

  @Override
  public int hashCode() {
    return Objects.hash(pharmacyId, medicineId);
  }
}
