package com.sprysa;

import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "post", schema = "storedpr_db", catalog = "")
public class PostEntity {

  private String post;

  @Id
  @Column(name = "post", nullable = false, length = 15)
  public String getPost() {
    return post;
  }

  public void setPost(String post) {
    this.post = post;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PostEntity that = (PostEntity) o;
    return Objects.equals(post, that.post);
  }

  @Override
  public int hashCode() {
    return Objects.hash(post);
  }
}
