package com.sprysa;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Id;

public class PharmacyMedicineEntityPK implements Serializable {

  private int pharmacyId;
  private int medicineId;

  @Column(name = "pharmacy_id", nullable = false)
  @Id
  public int getPharmacyId() {
    return pharmacyId;
  }

  public void setPharmacyId(int pharmacyId) {
    this.pharmacyId = pharmacyId;
  }

  @Column(name = "medicine_id", nullable = false)
  @Id
  public int getMedicineId() {
    return medicineId;
  }

  public void setMedicineId(int medicineId) {
    this.medicineId = medicineId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PharmacyMedicineEntityPK that = (PharmacyMedicineEntityPK) o;
    return pharmacyId == that.pharmacyId &&
        medicineId == that.medicineId;
  }

  @Override
  public int hashCode() {
    return Objects.hash(pharmacyId, medicineId);
  }
}
