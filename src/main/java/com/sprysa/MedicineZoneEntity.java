package com.sprysa;

import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "medicine_zone", schema = "storedpr_db", catalog = "")
@IdClass(MedicineZoneEntityPK.class)
public class MedicineZoneEntity {

  private int medicineId;
  private int zoneId;

  @Id
  @Column(name = "medicine_id", nullable = false)
  public int getMedicineId() {
    return medicineId;
  }

  public void setMedicineId(int medicineId) {
    this.medicineId = medicineId;
  }

  @Id
  @Column(name = "zone_id", nullable = false)
  public int getZoneId() {
    return zoneId;
  }

  public void setZoneId(int zoneId) {
    this.zoneId = zoneId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    MedicineZoneEntity that = (MedicineZoneEntity) o;
    return medicineId == that.medicineId &&
        zoneId == that.zoneId;
  }

  @Override
  public int hashCode() {
    return Objects.hash(medicineId, zoneId);
  }
}
