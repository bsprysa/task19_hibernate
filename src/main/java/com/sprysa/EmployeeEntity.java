package com.sprysa;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "employee", schema = "storedpr_db", catalog = "")
public class EmployeeEntity {

  private int id;
  private String surname;
  private String name;
  private String midleName;
  private String identityNumber;
  private String passport;
  private BigDecimal experience;
  private Date birthday;
  private String post;
  private Integer pharmacyId;

  @Id
  @Column(name = "id", nullable = false)
  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  @Basic
  @Column(name = "surname", nullable = false, length = 30)
  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  @Basic
  @Column(name = "name", nullable = false, length = 30)
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Basic
  @Column(name = "midle_name", nullable = true, length = 30)
  public String getMidleName() {
    return midleName;
  }

  public void setMidleName(String midleName) {
    this.midleName = midleName;
  }

  @Basic
  @Column(name = "identity_number", nullable = true, length = 10)
  public String getIdentityNumber() {
    return identityNumber;
  }

  public void setIdentityNumber(String identityNumber) {
    this.identityNumber = identityNumber;
  }

  @Basic
  @Column(name = "passport", nullable = true, length = 10)
  public String getPassport() {
    return passport;
  }

  public void setPassport(String passport) {
    this.passport = passport;
  }

  @Basic
  @Column(name = "experience", nullable = true, precision = 1)
  public BigDecimal getExperience() {
    return experience;
  }

  public void setExperience(BigDecimal experience) {
    this.experience = experience;
  }

  @Basic
  @Column(name = "birthday", nullable = true)
  public Date getBirthday() {
    return birthday;
  }

  public void setBirthday(Date birthday) {
    this.birthday = birthday;
  }

  @Basic
  @Column(name = "post", nullable = false, length = 15)
  public String getPost() {
    return post;
  }

  public void setPost(String post) {
    this.post = post;
  }

  @Basic
  @Column(name = "pharmacy_id", nullable = true)
  public Integer getPharmacyId() {
    return pharmacyId;
  }

  public void setPharmacyId(Integer pharmacyId) {
    this.pharmacyId = pharmacyId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    EmployeeEntity that = (EmployeeEntity) o;
    return id == that.id &&
        Objects.equals(surname, that.surname) &&
        Objects.equals(name, that.name) &&
        Objects.equals(midleName, that.midleName) &&
        Objects.equals(identityNumber, that.identityNumber) &&
        Objects.equals(passport, that.passport) &&
        Objects.equals(experience, that.experience) &&
        Objects.equals(birthday, that.birthday) &&
        Objects.equals(post, that.post) &&
        Objects.equals(pharmacyId, that.pharmacyId);
  }

  @Override
  public int hashCode() {
    return Objects
        .hash(id, surname, name, midleName, identityNumber, passport, experience, birthday, post,
            pharmacyId);
  }
}
