package com.sprysa;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Id;

public class MedicineZoneEntityPK implements Serializable {

  private int medicineId;
  private int zoneId;

  @Column(name = "medicine_id", nullable = false)
  @Id
  public int getMedicineId() {
    return medicineId;
  }

  public void setMedicineId(int medicineId) {
    this.medicineId = medicineId;
  }

  @Column(name = "zone_id", nullable = false)
  @Id
  public int getZoneId() {
    return zoneId;
  }

  public void setZoneId(int zoneId) {
    this.zoneId = zoneId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    MedicineZoneEntityPK that = (MedicineZoneEntityPK) o;
    return medicineId == that.medicineId &&
        zoneId == that.zoneId;
  }

  @Override
  public int hashCode() {
    return Objects.hash(medicineId, zoneId);
  }
}
