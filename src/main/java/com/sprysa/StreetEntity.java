package com.sprysa;

import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "street", schema = "storedpr_db", catalog = "")
public class StreetEntity {

  private String street;

  @Id
  @Column(name = "street", nullable = false, length = 25)
  public String getStreet() {
    return street;
  }

  public void setStreet(String street) {
    this.street = street;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    StreetEntity that = (StreetEntity) o;
    return Objects.equals(street, that.street);
  }

  @Override
  public int hashCode() {
    return Objects.hash(street);
  }
}
