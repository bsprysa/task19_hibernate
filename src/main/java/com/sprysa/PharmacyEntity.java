package com.sprysa;

import java.sql.Time;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "pharmacy", schema = "storedpr_db", catalog = "")
public class PharmacyEntity {

  private int id;
  private String name;
  private String buildingNumber;
  private String www;
  private Time workTime;
  private Boolean saturday;
  private Boolean sunday;
  private String street;

  @Id
  @Column(name = "id", nullable = false)
  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  @Basic
  @Column(name = "name", nullable = false, length = 15)
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Basic
  @Column(name = "building_number", nullable = true, length = 10)
  public String getBuildingNumber() {
    return buildingNumber;
  }

  public void setBuildingNumber(String buildingNumber) {
    this.buildingNumber = buildingNumber;
  }

  @Basic
  @Column(name = "www", nullable = true, length = 40)
  public String getWww() {
    return www;
  }

  public void setWww(String www) {
    this.www = www;
  }

  @Basic
  @Column(name = "work_time", nullable = true)
  public Time getWorkTime() {
    return workTime;
  }

  public void setWorkTime(Time workTime) {
    this.workTime = workTime;
  }

  @Basic
  @Column(name = "saturday", nullable = true)
  public Boolean getSaturday() {
    return saturday;
  }

  public void setSaturday(Boolean saturday) {
    this.saturday = saturday;
  }

  @Basic
  @Column(name = "sunday", nullable = true)
  public Boolean getSunday() {
    return sunday;
  }

  public void setSunday(Boolean sunday) {
    this.sunday = sunday;
  }

  @Basic
  @Column(name = "street", nullable = true, length = 25)
  public String getStreet() {
    return street;
  }

  public void setStreet(String street) {
    this.street = street;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PharmacyEntity that = (PharmacyEntity) o;
    return id == that.id &&
        Objects.equals(name, that.name) &&
        Objects.equals(buildingNumber, that.buildingNumber) &&
        Objects.equals(www, that.www) &&
        Objects.equals(workTime, that.workTime) &&
        Objects.equals(saturday, that.saturday) &&
        Objects.equals(sunday, that.sunday) &&
        Objects.equals(street, that.street);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, buildingNumber, www, workTime, saturday, sunday, street);
  }
}
